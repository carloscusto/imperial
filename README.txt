# Creación de un tema para Drupal 8.x

## Enlaces de interés

 - El tema está basado en el template "Imperial - One Page Bootstrap Template Free". que lo pueden descargar desde aquí: [https://themewagon.com/themes/one-page-bootstrap-template-free-download/](https://themewagon.com/themes/one-page-bootstrap-template-free-download/)
 - Documentación oficial de Drupal 8 para Theming: [https://www.drupal.org/docs/8/theming](https://www.drupal.org/docs/8/theming)
